package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import base.DBmanager;

public class ShipppingAddressDAO {
	public static void insertShippingAddress(String id, String lastName, String firstName, String lastNameKatakana,
			String firstNameKatakana, String mailAddress, String postalCode, String prefectures, String cities,
			String buildingName, String phoneNumber) {
		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBmanager.getConnection();
			st = con.prepareStatement(
					"INSERT INTO t_shipping_address(user_id,last_name,first_name,last_name_katakana,first_name_katakana,mail_address,postal_code,prefectures,cities,building_name,phone_number)"
					+ "VALUES(?,?,?,?,?,?,?,?,?,?,?)");

			st.setString(1, id);
			st.setString(2, lastName);
			st.setString(3, firstName);
			st.setString(4, lastNameKatakana);
			st.setString(5, firstNameKatakana);
			st.setString(6, mailAddress);
			st.setString(7, postalCode);
			st.setString(8, prefectures);
			st.setString(9, cities);
			st.setString(10, buildingName);
			st.setString(11, phoneNumber);
			int result = st.executeUpdate();
			System.out.println("inserting Shipping_Address has been completed");

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
}

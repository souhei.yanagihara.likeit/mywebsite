package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.xml.bind.DatatypeConverter;

import base.DBmanager;
import beans.UserDataBeans;

public class UserDAO {

	//データの挿入処理
	public static void insert(String name, String address, String loginId, String loginPassword, String birthday) {
		Connection con = null;
		PreparedStatement pStmt = null;

		try {
			con = DBmanager.getConnection();
			pStmt = con.prepareStatement(
					"INSERT INTO t_user(name,address,login_id,login_password,birthday,create_date) VALUES(?,?,?,?,?,NOW())");

			//パスワード暗号化
			String encryptionPass = encryption(loginPassword);
			pStmt.setString(1, name);
			pStmt.setString(2, address);
			pStmt.setString(3, loginId);
			pStmt.setString(4, encryptionPass);
			pStmt.setString(5, birthday);
			int result = pStmt.executeUpdate();
			System.out.println("inserting user has been completed");
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	//ユーザーIDの取得
	public UserDataBeans getUserId(String loginId, String loginIdPassword) {
		Connection con = null;
		try {
			con = DBmanager.getConnection();
			PreparedStatement pStmt = con.prepareStatement("SELECT*FROM t_user WHERE login_id=? and login_password=?");

			String encryptionPass = encryption(loginIdPassword);
			pStmt.setString(1, loginId);
			pStmt.setString(2, encryptionPass);

			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			String loginIdDate = rs.getString("login_id");
			String nameDate = rs.getString("name");
			int userIdDate = rs.getInt("id");
			String userPassword = rs.getString("login_password");

			return new UserDataBeans(loginIdDate, nameDate, userIdDate);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}

		}
	}

	//ユーザーIDからユーザー情報を取得する
	public UserDataBeans getUserDataByUserId(String userId) {
		Connection con = null;
		try {
			con = DBmanager.getConnection();
			PreparedStatement st = con.prepareStatement(
					"SELECT id,name,login_id,address,birthday,create_date FROM t_user WHERE id=?");
			st.setString(1, userId);

			ResultSet rs = st.executeQuery();

			if (!rs.next()) {
				return null;
			}

			int Userid = rs.getInt("id");
			String Username = rs.getString("name");
			String LoginId = rs.getString("login_id");
			String Address = rs.getString("address");
			String Birthday = rs.getString("birthday");
			String Createday = rs.getString("create_date");

			return new UserDataBeans(Userid, Username, LoginId, Address, Birthday, Createday);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}

		}
	}

	//loginIdの重複チェック
	public static boolean isOverlapLoginId(String loginId, int userId) throws SQLException {
		// 重複しているかどうか表す変数
		boolean isOverlap = false;
		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBmanager.getConnection();
			// 入力されたlogin_idが存在するか調べる
			st = con.prepareStatement("SELECT login_id FROM t_user WHERE login_id = ? AND id != ?");
			st.setString(1, loginId);
			st.setInt(2, userId);
			ResultSet rs = st.executeQuery();

			System.out.println("searching loginId by inputLoginId has been completed");

			if (rs.next()) {
				isOverlap = true;
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}

		System.out.println("overlap check has been completed");
		return isOverlap;
	}

	//ユーザー情報の更新
	public static void updataUser(String id, String name, String loginId, String address, String loginPassword) {
		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBmanager.getConnection();
			st = con.prepareStatement("UPDATE t_user SET name=?,login_id=?,address=?,login_password=? WHERE id=?");

			//パスワード暗号化
			String encryptionPass = encryption(loginPassword);
			st.setString(1, name);
			st.setString(2, loginId);
			st.setString(3, address);
			st.setString(4, encryptionPass);
			st.setString(5, id);
			int result = st.executeUpdate();
			System.out.println("searching updated-UserDataBeans has been completed");

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	//ユーザー情報テーブルから削除
	public static void UserDataDeleteByUserId(String Id) {
		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBmanager.getConnection();

			st = con.prepareStatement("DELETE FROM t_user WHERE id=?");

			st.setString(1, Id);

			int result = st.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	//暗号化メソッド
	public static String encryption(String loginPassword) {
		//ハッシュを生成したい元の文字列
		String source = loginPassword;
		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;
		//ハッシュアルゴリズム
		String algorithm = "MD5";

		//ハッシュ生成処理
		byte[] bytes = null;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		String result = DatatypeConverter.printHexBinary(bytes);

		//標準出力
		return result;

	}
}

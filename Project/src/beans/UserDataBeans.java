package beans;

import java.io.Serializable;

//ユーザー情報のBeans

public class UserDataBeans implements Serializable {

	private int id;
	private String name;
	private String address;
	private String loginId;
	private String loginIdPassword;
	private String birthday;
	private String createday;

	//コンストラクタ
	public UserDataBeans() {
		this.name = "";
		this.address = "";
		this.loginId = "";
		this.loginIdPassword = "";
	}

	public UserDataBeans(String loginId, String name, int id) {
		this.loginId = loginId;
		this.name = name;
		this.id = id;
	}

	public UserDataBeans(String name, String loginId, String address, String loginIdPassword, String birthday) {
		this.name = name;
		this.loginId = loginId;
		this.address = address;
		this.loginIdPassword = loginIdPassword;
		this.birthday = birthday;
	}

	public UserDataBeans(int id, String name, String loginId, String address, String birthday, String createday) {
		this.id = id;
		this.name = name;
		this.loginId = loginId;
		this.address = address;
		this.birthday = birthday;
		this.createday = createday;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getLoginIdPassword() {
		return loginIdPassword;
	}

	public void setLoginIdPassword(String loginIdPassword) {
		this.loginIdPassword = loginIdPassword;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getCreateday() {
		return createday;
	}

	public void setCreateday(String createday) {
		this.createday = createday;
	}
}
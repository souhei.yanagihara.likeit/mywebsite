package beans;

import java.io.Serializable;

//配送先住所
public class ShippingAddressDataBeans implements Serializable {
	private int id;
	private int userId;
	private String lastName;
	private String firstName;
	private String lastNameKatakana;
	private String firstNameKatakana;
	private String mailAddress;
	private int postalCode;
	private String prefectures;
	private String cities;
	private String buildingName;
	private int phoneNumber;

	//コンストラクタ
	public ShippingAddressDataBeans() {

	}

	public ShippingAddressDataBeans(int userId, String lastName, String firstName, String lastNameKatakana,
			String firstNameKatakana, String mailAddress, int postalCode, String prefectures, String cities,
			String buildinName, int phoneNumber) {

			this.userId = userId;
			this.lastName = lastName;
			this.firstName = firstName;
			this.lastNameKatakana = lastNameKatakana;
			this.firstNameKatakana = firstNameKatakana;
			this.mailAddress = mailAddress;
			this.postalCode = postalCode;
			this.prefectures = prefectures;
			this.cities = cities;
			this.buildingName = buildinName;
			this.phoneNumber = phoneNumber;

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastNameKatakana() {
		return lastNameKatakana;
	}

	public void setLastNameKatakana(String lastNameKatakana) {
		this.lastNameKatakana = lastNameKatakana;
	}

	public String getFirstNameKatakana() {
		return firstNameKatakana;
	}

	public void setFirstNameKatakana(String firstNameKatakana) {
		this.firstNameKatakana = firstNameKatakana;
	}

	public String getMailAddress() {
		return mailAddress;
	}

	public void setMailAddress(String mailAddress) {
		this.mailAddress = mailAddress;
	}

	public int getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(int postalCode) {
		this.postalCode = postalCode;
	}

	public String getPrefectures() {
		return prefectures;
	}

	public void setPrefectures(String prefectures) {
		this.prefectures = prefectures;
	}

	public String getCities() {
		return cities;
	}

	public void setCities(String cities) {
		this.cities = cities;
	}

	public String getBuildingName() {
		return buildingName;
	}

	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}

	public int getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(int phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

}

package ec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ShippingAddressDataBeans;

/**
 * Servlet implementation class ShippingAddreassConfirm
 */
@WebServlet("/ShippingAddreassConfirm")
public class ShippingAddreassConfirm extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ShippingAddreassConfirm() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/* 文字化け対策 */
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		//リクエストゲットパラメータから値を取得
		String id = request.getParameter("id");
		String lastName = request.getParameter("lastName");
		String firstName = request.getParameter("firstName");
		String lastNameKatakana = request.getParameter("lastNameKatakana");
		String firstNameKatakana = request.getParameter("firstNameKatakana");
		String mail = request.getParameter("email");
		String postCode = request.getParameter("zip01");
		String todouhuken = request.getParameter("pref01");
		String sityouson = request.getParameter("addr01");
		String building = request.getParameter("building");
		String phoneNumber = request.getParameter("phone_number");

		//ShippingAddressDataBeansにセット
		ShippingAddressDataBeans sadb = new ShippingAddressDataBeans();
		sadb.setId(Integer.parseInt(id));
		sadb.setLastName(lastName);
		sadb.setFirstName(firstName);
		sadb.setLastNameKatakana(lastNameKatakana);
		sadb.setFirstNameKatakana(firstNameKatakana);
		sadb.setMailAddress(mail);
		sadb.setPostalCode(Integer.parseInt(postCode));
		sadb.setPrefectures(todouhuken);
		sadb.setCities(sityouson);
		sadb.setBuildingName(building);
		sadb.setPhoneNumber(Integer.parseInt(phoneNumber));

		request.setAttribute("sadb", sadb);
		RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/shipping_address_confirm.jsp");
		dispatcher.forward(request, response);
	}

}

package ec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ShippingAddressDataBeans;
import dao.ShipppingAddressDAO;

/**
 * Servlet implementation class ShippingAddressResult
 */
@WebServlet("/ShippingAddressResult")
public class ShippingAddressResult extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ShippingAddressResult() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/*文字化け防止対策*/
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		//リクエストゲットパラメータから値を取得
		String id = request.getParameter("id");
		String lastName = request.getParameter("lastName");
		String firstName = request.getParameter("firstName");
		String lastNameKatakana = request.getParameter("lastNameKatakana");
		String firstNameKatakana = request.getParameter("firstNameKatakana");
		String mail = request.getParameter("email");
		String postCode = request.getParameter("post_code");
		String todouhuken = request.getParameter("prefectures");
		String sityouson = request.getParameter("cities");
		String building = request.getParameter("building");
		String phoneNumber = request.getParameter("phone_number");

		//ShippingAddressDataBeansにセット
		ShippingAddressDataBeans sadb = new ShippingAddressDataBeans();
		sadb.setUserId(Integer.parseInt(id));
		sadb.setLastName(lastName);
		sadb.setFirstName(firstName);
		sadb.setLastNameKatakana(lastNameKatakana);
		sadb.setFirstNameKatakana(firstNameKatakana);
		sadb.setMailAddress(mail);
		sadb.setPostalCode(Integer.parseInt(postCode));
		sadb.setPrefectures(todouhuken);
		sadb.setCities(sityouson);
		sadb.setBuildingName(building);
		sadb.setPhoneNumber(Integer.parseInt(phoneNumber));

		// 登録が確定されたかどうか確認するための変数
		String confirmed = request.getParameter("confirm_button");

		switch (confirmed) {
		case "cacel":
			session.setAttribute("sadb", sadb);
			response.sendRedirect("ShippingAddressRegist");
			break;

		case "regist":
			ShipppingAddressDAO.insertShippingAddress(id, lastName, firstName, lastNameKatakana, firstNameKatakana,
					mail, postCode, todouhuken, sityouson, building, phoneNumber);
			session.setAttribute("sadb", sadb);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/shipping_address_result.jsp");
			dispatcher.forward(request, response);
			break;
		}

	}

}

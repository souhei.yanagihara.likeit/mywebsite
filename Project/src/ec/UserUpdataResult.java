package ec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDAO;

/**
 * Servlet implementation class UserUpdataResult
 */
@WebServlet("/UserUpdataResult")
public class UserUpdataResult extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserUpdataResult() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/*文字化け防止対策*/
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		String id = request.getParameter("id");
		String name = request.getParameter("name");
		String loginId = request.getParameter("loginId");
		String address = request.getParameter("address");
		String password = request.getParameter("password");

		UserDataBeans udb = new UserDataBeans();
		udb.setId(Integer.parseInt(id));
		udb.setName(name);
		udb.setLoginId(loginId);
		udb.setAddress(address);
		udb.setLoginIdPassword(password);

		// 登録が確定されたかどうか確認するための変数
		String confirmed = request.getParameter("confirm_button");

		switch (confirmed) {
		case "cancel":
			session.setAttribute("udb", udb);
			response.sendRedirect("UserUpdata");
			break;

		case "updata":
			UserDAO.updataUser(id, name, loginId, address, password);
			session.setAttribute("udb", udb);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_updata_result.jsp");
			dispatcher.forward(request, response);
			break;
		}
	}

}

package ec;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;

/**
 * Servlet implementation class ItemDelete
 */
@WebServlet("/ItemDelete")
public class ItemDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ItemDelete() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();

		String[] deleteItemIdList = request.getParameterValues("delete_item_id_list");
		ArrayList<ItemDataBeans> cart = (ArrayList<ItemDataBeans>) session.getAttribute("cart");
		int totalprice = (int) session.getAttribute("totalprice");
		int totalCount = (int) session.getAttribute("totalCount");

		String cartActionMessage = "";
		if (deleteItemIdList != null) {
			//削除対象の商品を削除
			for (String deleteItemId : deleteItemIdList) {
				for (ItemDataBeans cartInItem : cart) {
					if (cartInItem.getId() == Integer.parseInt(deleteItemId)) {
						totalprice -= cartInItem.getPrice();
						totalCount -= Integer.parseInt(cartInItem.getCount());
						cart.remove(cartInItem);
						break;
					}
				}
			}

		}
		response.sendRedirect("Cart");
	}

}

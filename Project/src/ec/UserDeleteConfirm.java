package ec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;

/**
 * Servlet implementation class UserDeleteConfirm
 */
@WebServlet("/UserDeleteConfirm")
public class UserDeleteConfirm extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/* 文字化け対策 */
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		String id = request.getParameter("id");
		String name = request.getParameter("name");
		String loginId = request.getParameter("loginId");
		String address = request.getParameter("address");

		UserDataBeans udb = new UserDataBeans();
		udb.setId(Integer.parseInt(id));
		udb.setName(name);
		udb.setLoginId(loginId);
		udb.setAddress(address);

		request.setAttribute("udb", udb);
		RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/user_delete_confirm.jsp");
		dispatcher.forward(request, response);
	}

}

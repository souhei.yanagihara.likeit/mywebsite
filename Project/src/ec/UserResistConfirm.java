package ec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDAO;

/**
 * Servlet implementation class UserResistConfirm
 */
@WebServlet("/UserResistConfirm")
public class UserResistConfirm extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserResistConfirm() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/* 文字化け対策 */
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		try {
			String Username = request.getParameter("user_name");
			String Useraddress = request.getParameter("address");
			String LoginId = request.getParameter("login_id");
			String Password = request.getParameter("password");
			String ConfirmPassword = request.getParameter("confirm_password");
			String UserBirthday = request.getParameter("birth_day");

			UserDataBeans udb = new UserDataBeans();
			udb.setName(Username);
			udb.setAddress(Useraddress);
			udb.setLoginId(LoginId);
			udb.setLoginIdPassword(Password);
			udb.setBirthday(UserBirthday);

			String errMessage = "";

			// 入力されているパスワードが確認用と等しいか
			if (!Password.equals(ConfirmPassword)) {
				errMessage += "入力されているパスワードと確認用パスワードが違います";
			}

			//ログインIDの重複チェック
			if (UserDAO.isOverlapLoginId(udb.getLoginId(), 0)) {
				errMessage += "ほかのユーザーが使用中のログインIDです";
			}

			// バリデーションエラーメッセージがないなら確認画面へ
			if (errMessage.length() == 0) {
				request.setAttribute("udb", udb);
				RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/user_resistconfirm.jsp");
				dispatcher.forward(request, response);
			} else {
				session.setAttribute("udb", udb);
				session.setAttribute("errMessage", errMessage);
				response.sendRedirect("UserResist");
			}

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

}

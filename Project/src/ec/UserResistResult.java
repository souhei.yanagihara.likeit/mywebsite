package ec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDAO;

/**
 * Servlet implementation class UserResistResult
 */
@WebServlet("/UserResistResult")
public class UserResistResult extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserResistResult() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/*文字化け防止対策*/
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		try {
			String Username = request.getParameter("name");
			String Useraddress = request.getParameter("address");
			String LoginId = request.getParameter("login_Id");
			String Password = request.getParameter("password");
			String UserBirthday = request.getParameter("birth_day");

			UserDataBeans udb = new UserDataBeans();
			udb.setName(Username);
			udb.setAddress(Useraddress);
			udb.setLoginId(LoginId);
			udb.setLoginIdPassword(Password);
			udb.setBirthday(UserBirthday);

			// 登録が確定されたかどうか確認するための変数
			String confirmed = request.getParameter("confirm_button");

			switch (confirmed) {
			case "cacel":
				session.setAttribute("udb", udb);
				response.sendRedirect("UserResist");
				break;

			case "regist":
				UserDAO.insert(Username, Useraddress, LoginId, Password, UserBirthday);
				session.setAttribute("udb", udb);
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_resistresult.jsp");
				dispatcher.forward(request, response);
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");

		}
	}
}

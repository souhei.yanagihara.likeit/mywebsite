package ec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDAO;

/**
 * Servlet implementation class UserDeleteResult
 */
@WebServlet("/UserDeleteResult")
public class UserDeleteResult extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserDeleteResult() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/*文字化け防止対策*/
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		String id = request.getParameter("id");
		String name = request.getParameter("name");
		String loginId = request.getParameter("loginId");
		String address = request.getParameter("address");

		UserDataBeans udb = new UserDataBeans();
		udb.setId(Integer.parseInt(id));
		udb.setName(name);
		udb.setLoginId(loginId);
		udb.setAddress(address);

		// 登録が確定されたかどうか確認するための変数
		String confirmed = request.getParameter("confirm_button");

		switch (confirmed) {
		case "cancel":
			session.setAttribute("udb", udb);
			response.sendRedirect("UserDelete");
			break;

		case "delete":
			UserDAO.UserDataDeleteByUserId(id);
			session.setAttribute("udb", udb);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_delete_result.jsp");
			dispatcher.forward(request, response);
			break;
		}
	}

}

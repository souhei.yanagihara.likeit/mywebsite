package ec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;

/**
 * Servlet implementation class UserResist
 */
@WebServlet("/UserResist")
public class UserResist extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserResist() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();

		String errMessage = (String) session.getAttribute("errMessage");
		UserDataBeans udb = (UserDataBeans) session.getAttribute("udb");

		request.setAttribute("udb", udb);
		request.setAttribute("errMessage", errMessage);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_resist.jsp");
		dispatcher.forward(request, response);
	}

}

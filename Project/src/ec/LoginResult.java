package ec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDAO;

/**
 * Servlet implementation class LoginResult
 */
@WebServlet("/LoginResult")
public class LoginResult extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginResult() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();

		//リクエストパラメータの入力項目を取得
		String loginId = request.getParameter("login_id");
		String password = request.getParameter("password");

		//リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		UserDAO UserDao = new UserDAO();
		UserDataBeans user = UserDao.getUserId(loginId, password);

		//エラーメッセージ
		if (user == null) {
			session.setAttribute("errorMessage", "ログインIDまたはパスワードが異なります");
			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
			return;
		}

		session.setAttribute("userInfo", user);
		response.sendRedirect("Index");

	}
}

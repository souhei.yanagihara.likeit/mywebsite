package ec;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;

/**
 * Servlet implementation class Cart
 */
@WebServlet("/Cart")
public class Cart extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Cart() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();

		//カートを取得
		ArrayList<ItemDataBeans> cart = (ArrayList<ItemDataBeans>) session.getAttribute("cart");

		//セッションにカートがない場合カートを作成
		if (cart == null) {
			cart = new ArrayList<ItemDataBeans>();
			session.setAttribute("cart", cart);
		}

		//カートに追加した商品の合計金額（配送料を除く）と合計数
		int totalprice = 0;
		int totalCount = 0;
		for (ItemDataBeans addItem : cart) {
			totalCount += Integer.parseInt(addItem.getCount());
			totalprice += addItem.getPrice() *Integer.parseInt(addItem.getCount());
		}

		String cartActionMessage = "";
		//カートに商品が入っていないなら
		if (cart.size() == 0) {
			cartActionMessage = "カートに商品がありません";
		}

		session.setAttribute("totalCount", totalCount);
		session.setAttribute("totalprice", totalprice);
		request.setAttribute("cartActionMessage", cartActionMessage);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/cart.jsp");
		dispatcher.forward(request, response);

	}

}

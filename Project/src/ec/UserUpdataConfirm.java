package ec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;

/**
 * Servlet implementation class UserUpdataConfirm
 */
@WebServlet("/UserUpdataConfirm")
public class UserUpdataConfirm extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserUpdataConfirm() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/* 文字化け対策 */
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		String id = request.getParameter("id");
		String name = request.getParameter("name");
		String loginId = request.getParameter("loginId");
		String address = request.getParameter("address");
		String password = request.getParameter("password");
		String confirmPassword = request.getParameter("passwordConfirm");

		UserDataBeans udb = new UserDataBeans();
		udb.setId(Integer.parseInt(id));
		udb.setName(name);
		udb.setAddress(address);
		udb.setLoginId(loginId);
		udb.setLoginIdPassword(password);

		String errorMessage = "";

		// 入力されているパスワードが確認用と等しいか
		if (!password.equals(confirmPassword)) {
			errorMessage += "入力されているパスワードと確認用パスワードが違います";
		}

		// エラーメッセージがないなら確認画面へ
		if (errorMessage.length() == 0) {
			request.setAttribute("udb", udb);
			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/user_updata_confirm.jsp");
			dispatcher.forward(request, response);
		} else {
			session.setAttribute("errorMessage", errorMessage);
			response.sendRedirect("UserUpdata");
		}

	}
}

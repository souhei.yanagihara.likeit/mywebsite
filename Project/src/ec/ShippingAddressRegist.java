package ec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ShippingAddressDataBeans;
import beans.UserDataBeans;
import dao.UserDAO;

/**
 * Servlet implementation class ShippingAddressRegist
 */
@WebServlet("/ShippingAddressRegist")
public class ShippingAddressRegist extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ShippingAddressRegist() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();

		// URLからGETパラメータとしてIDを受け取る
		String id = request.getParameter("id");

		//リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		UserDAO user = new UserDAO();
		UserDataBeans userId = user.getUserDataByUserId(id);

		ShippingAddressDataBeans sadb = (ShippingAddressDataBeans) session.getAttribute("sadb");

		//リクエストスコープにインスタンスを保存
		request.setAttribute("user", userId);
		request.setAttribute("sadb", sadb);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/shipping_address_regist.jsp");
		dispatcher.forward(request, response);

	}

}

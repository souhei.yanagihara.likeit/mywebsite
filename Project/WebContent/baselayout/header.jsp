<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title></title>
<link rel="stylesheet" href="css/header.css">
</head>
<body>
	<header>
		<h1 class="headline">
			<a href="Index" id="home-tag">ECサイト</a>
		</h1>
		<ul class="nav-list">
			<%-- 管理者がログインしているときに限り表示される--%>
			<a href="Admin"><li class="nav-list-item" id="home-tag">admin</li></a>
			<li class="nav-list-item"><a href="UserData" id="home-tag">user</a>
			</li>
			<a href="Cart"><li class="nav-list-item" id="home-tag">cart</li></a>
			<a href="Logout"><li class="nav-list-item" id="home-tag">logout</li></a>
		</ul>
	</header>
</body>
</html>
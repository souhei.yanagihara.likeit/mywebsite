<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>TOPページ</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/header.css">
<link rel="stylesheet" href="css/footer.css">
<link rel="stylesheet" href="css/cart.css">
<link rel="stylesheet" href="css/checkbox.css">
<link rel="stylesheet" href="css/index.css">
<link rel="stylesheet" href="css/search.css">
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />
	<main role="main">
	<div id="search-area">
		<div class="container">
			<h2 id="site-name">サイト名</h2>
			<form id="form1" action="" method="">
				<input id="sbox" id="s" name="search_word" type="text" placeholder="キーワードを入力" /> <input id="sbtn" type="submit"
					value="検索" />
			</form>
		</div>
	</div>

	<div id="side-bar">
		<div class="container">
			<nav id="side-section">
				<ul id="side-area">
					<li>ユーザー
						<ol id="option">
							<li id="list"><a href="UserDetail?id=${userId.id }">詳細</a></li>
							<li id="list"><a href="UserUpdata?id=${userId.id }">編集</a></li>
							<li id="list"><a href="UserDelete?id=${userId.id }">削除</a></li>
						</ol>
					</li>
					<li>配送先
						<ol id="option">
							<li id="list"><a
								href="ShippingAddressManagement?id=${userId.id }">詳細</a></li>
							<li id="list"><a
								href="ShippingAddressRegist?id=${userId.id }">追加</a></li>
						</ol>
					</li>
					<li>お支払い
						<ol id="option">
							<li id="list"><a href="PaymentManagement?id=${userId.id }">詳細</a></li>
							<li id="list"><a href="PaymentResist?id=${userId.id }">追加</a></li>
						</ol>
					</li>
				</ul>
			</nav>
		</div>
	</div>

	<div id="item-area">
		<div class="section">
			<h3>おすすめ商品</h3>
			<br>
			<div class="row">
				<c:forEach var="item" items="${itemList}">
					<div class="col-4">
						<div class="card">
							<a href="Item?item_id=${item.id}">
								<img class="cardImg" src="img/${item.fileName}">
							</a>
							<div class="item-body">
								<p class="item-name">${item.name }</p>
								<div class="d-flex justify-content-between align-items-center">
									<p class="item-price">${item.formatPrice }円</p>
								</div>
							</div>
						</div>
					</div>
				</c:forEach>
			</div>
		</div>
	</div>
<br>


	<div id="item-area">
		<div class="section">
			<h3>過去に購入した商品</h3>
			<br>
			<div class="row">
				<div class="col-4">
					<div class="card mb-4 shadow-sm">
						<a href="item.html"><svg
								class="bd-placeholder-img card-img-top" width="100%"
								height="225" xmlns="http://www.w3.org/2000/svg"
								preserveAspectRatio="xMidYMid slice" focusable="false"
								role="img" aria-label="Placeholder: Thumbnail">
                    <rect width="100%" height="100%" fill="#55595c" />
                    <text x="50%" y="50%" fill="#eceeef" dy=".3em">商品の画像1</text>
                </svg></a>
						<div class="item-body">
							<p class="item-name">商品名</p>
							<div class="d-flex justify-content-between align-items-center">
								<p class="item-price">○○円</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	</main>
	<jsp:include page="/baselayout/footer.jsp" />
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー情報/削除</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/header.css">
<link rel="stylesheet" href="css/footer.css">
<link rel="stylesheet" href="css/resist.css">
<link rel="stylesheet" href="css/cart.css">
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />
	<div class="container">
		<div class="col-6 mx-auto">
			<div class="col10 mx-auto">
				<h2>ユーザー情報</h2>
				<h5>以下のユーザー情報を削除します。</h5>
				<form action="UserDeleteConfirm" method="post">

					<%--ユーザーIDをtype="hidden"で取得 --%>
					<input type="hidden" name="id" value=${user.id } >

					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text" id="basic-addon1">ユーザー名</span>
						</div>
					<input type="text" class="form-control" name="name" value=${user.name }  readonly>
					</div>

					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text" id="basic-addon1">ログインID</span>
						</div>
						<input type="text" class="form-control" name="loginId" value=${user.loginId }  readonly>
					</div>

					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text" id="basic-addon1">住所</span>
						</div>
						<input type="text" class="form-control" name="address" value=${user.address }  readonly>
					</div>

					<hr class="mb-4">
					<p>
						<button class="btn-flat-simple btn-lg bin btn-lg btn-block" type="submit">確認</button>
					</p>
				</form>
			</div>
		</div>
	</div>
	<jsp:include page="/baselayout/footer.jsp" />
</body>
</html>
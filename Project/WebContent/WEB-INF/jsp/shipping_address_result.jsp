<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>配送先住所/登録完了</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<script src="https://ajaxzip3.github.io/ajaxzip3.js" charset="UTF-8"></script>
<link rel="stylesheet" href="css/header.css">
<link rel="stylesheet" href="css/footer.css">
<link rel="stylesheet" href="css/normal.css">
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />
	<div class="container">
		<div class="col-6 mx-auto">
			<div class="col10 mx-auto">
				<h2 class="mb-3">登録が完了しました</h2>

					<div class="row">
						<div class="col-md-5 mb-3">
							<label for="lastName">姓</label>
							<input type="text" name="" class="form-control" id="cc-lastName"  value="${sadb.lastName }" readonly>
						</div>
						<div class="col-md-6 mb-3">
							<label for="firstName">名</label>
							<input type="text" name="" class="form-control" id="cc-firstName" value="${sadb.firstName }" readonly>
						</div>
					</div>
					<div class="row">
						<div class="col-md-5 mb-3">
							<label for="lastName">セイ</label>
							<input type="text" name="" class="form-control" id="cc-lastName" value="${sadb.lastNameKatakana }" readonly>
						</div>
						<div class="col-md-6 mb-3">
							<label for="firstName">メイ</label>
							<input type="text" name="" class="form-control" id="cc-firstName" value="${sadb.firstNameKatakana }" readonly>
						</div>
					</div>
					<div class="mb-3">
						<label for="email">メールアドレス</label>
						<input type="email" class="form-control" id="email" value="${sadb.mailAddress }" readonly>
					</div>
					<div class="row">
						<div class="col-md-5 mb-3">
							<label for="postal-code">郵便番号</label>
							<input type="text" name="" class="form-control" id="cc-postalCode" value="${sadb.postalCode }" readonly>
						</div>
					</div>
					<div class="row">
						<div class="col-md-5 mb-3">
							<label for="address">都道府県</label>
							<input type="text" name="" class="form-control" id="address" value="${sadb.prefectures }" readonly>
						</div>
					</div>
					<div class="mb-3">
						<label for="address">都道府県以降の住所</label>
							<input type="text" class="form-control" id="address" value="${sadb.cities }" readonly>
					</div>
					<div class="mb-3">
						<label for="address">マンション等の建物名</label>
							<input type="text" class="form-control" id="address" value="${sadb.buildingName }" readonly>
					</div>
					<div class="mb-3">
						<label for="phone-number">電話番号</label>
							<input type="text" class="form-control" id="address" value="${sadb.phoneNumber }" readonly>
					</div>
					<hr class="mb-4">
					<div class="row">
						<div class="col s12">
							<h5>
								<div style="text-align: center">
									<p class="center-align">上記内容を登録しました</p>
								</div>
							</h5>
						</div>
					</div>
					<div class="row">
						<div class="col-9 mx-auto">
							<a href="Index" class="btn-flat-simple btn-lg btn btn-lg btn-block">ユーザー一覧へ</a>
						</div>
					</div>
			</div>
		</div>
	</div>
	<jsp:include page="/baselayout/footer.jsp" />
</body>
</html>
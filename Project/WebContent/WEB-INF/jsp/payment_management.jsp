<%@ page language="java" contentType="text/html; charset=UTF-8"pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>お支払い方法|管理</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" href="css/header.css">
<link rel="stylesheet" href="css/footer.css">
<link rel="stylesheet" href="css/input-group.css">
<link rel="stylesheet" href="css/normal.css">
    </head>
        <body>
<!-- header -->
<header>
  	<h1 class="headline">
      <a href="example.html" id="home-tag">ECサイト</a>
    </h1>
    <ul class="nav-list">
      <li class="nav-list-item">
        <a href="userdata.html" id="home-tag">user</a>
      </li>
        <a href="cart.html"><li class="nav-list-item" id="home-tag">cart</li></a>
        <a href="logout.html"><li class="nav-list-item" id="home-tag">logout</li></a>
    </ul>
</header>

<div class="container">
    <div class="col-10 mx-auto">
        <div class="col10 mx-auto">
            <h2 class="mb-3">お支払い機能の管理</h2>
                  <a href="userdata.html" class="btn-flat-simple btn-lg" >戻る</a>
<table class="table table-striped">
  <thead>
    <tr>
      <th ></th>
      <th >カードの種類</th>
      <th >名義人</th>
      <th >有効期限</th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>　
<c:forEach var="" items="" varStatus="status">
    <tr>
      <th scope="row"><a href="payment_detail.html">1</a></th>
      <td >クレジットカード</td>
      <td >佐藤</td>
      <td >2022/04</td>
      <td><a href="payment_updata.html" >編集</a></td>
      <td><a href="payment_delete.html">削除</a></td>
    </tr>

     <tr>
      <th scope="row"><a href="payment_detail.html">2</a></th>
      <td >デビットカード</td>
      <td >佐藤</td>
      <td >2026/10</td>

      <td><a href="payment_updata.html" >編集</a></td>
      <td><a href="payment_delete.html">削除</a></td>
    </tr>
</c:forEach>
  </tbody>
</table>
<a href="payment_resist.html" class="btn-flat-simple btn-lg btn btn-light btn-lg btn-block">お支払い方法を追加する</a>
        </div>
    </div>
</div>
<!-- footer -->
<footer>
　　<ul class="footer-menu">
     <li>home ｜</li>
     <li>about ｜</li>
     <li>service ｜</li>
     <li>Contact Us</li>
    </ul>
    <p>© All rights reserved by webcampnavi.</p>
</footer>
    </body>
</html>


<%@ page language="java" contentType="text/html; charset=UTF-8"pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>お支払い方法/登録</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" href="css/header.css">
<link rel="stylesheet" href="css/footer.css">
<link rel="stylesheet" href="css/normal.css">
<script src="https://ajaxzip3.github.io/ajaxzip3.js"></script>
    </head>
        <body>
<!-- header -->
<header>
  	<h1 class="headline">
      <a href="index.html" id="home-tag">ECサイト</a>
    </h1>
    <ul class="nav-list">
        <!--管理者がログインしているときに限り表示される-->
         <a href="admin.html"><li class="nav-list-item" id="home-tag">admin</li></a>
      <li class="nav-list-item">
        <a href="userdata.html" id="home-tag">user</a>
      </li>
        <a href="cart.html"><li class="nav-list-item" id="home-tag">cart</li></a>
        <a href="logout.html"><li class="nav-list-item" id="home-tag">logout</li></a>
    </ul>
</header>
            <div class="container">
                <div class="col-6 mx-auto">
                    <div class="col10 mx-auto">
                        <h2 class="mb-3">お支払い機能の追加</h2>

<form action="" method="">
    <h4 class="mb-3">・請求先住所</h4>
    <div class="row">
    <div class="col-md-5 mb-3">
        <label for="lastName">姓</label>
        <input type="text" name="" class="form-control" id="lastName" placeholder="例）山田"　value="" required>
        <div class="invalid-feedback">姓が未記入です</div>
    </div>
    <div cvlass="col-md-6 mb-3">
        <label for="firstName">名</label>
        <input type="text" name="" class="form-control" id="firstName" value="" placeholder="例）太郎" required>
        <div class="invalid-feedback">名が未記入です</div>
    </div>
    </div>
    <div class="row">
    <div class="col-md-5 mb-3">
        <label for="lastName">セイ</label>
        <input type="text" name="" class="form-control" id="lastName" value="" placeholder="例）ヤマダ" required>
        <div class="invalid-feedback">セイが未記入です</div>
    </div>
    <div cvlass="col-md-6 mb-3">
        <label for="firstName">メイ</label>
        <input type="text" name="" class="form-control" id="firstName" value="" placeholder="例）タロウ" required>
        <div class="invalid-feedback">メイが未記入です</div>
    </div>
    </div>
    <div class="mb-3">
        <label for="email">メールアドレス</label>
        <input type="email" name="" class="form-control" id="email" placeholder="you@example.com"　value="" required>
        <div class="invalid-feedback">メールアドレスが未記入です</div>
    </div>
    <div class="row">
    <div class="col-md-5 mb-3">
        <label>郵便番号(ハイフンでもOK)</label>
      <input type="text" name="zip01" size="10" maxlength="8" onKeyUp="AjaxZip3.zip2addr(this,'','pref01','addr01');"　value="" pattern="^[0-9A-Za-z]+$" required>
        <div class="invalid-feedback">郵便番号が未記入です</div>
    </div>
    </div>
    <div class="row">
    <div class="col-md-5 mb-3">
        <label for="address01">都道府県<span class="text-muted">(自動入力)</span></label>
         <input type="text" name="pref01" size="20"　value="" >
        <div class="invalid-feedback">住所(都道府県)が未記入です</div>
    </div>
    </div>
    <div class="mb-3">
        <label for="address02">都道府県以降の住所<span class="text-muted">(自動入力)</span></label>
        <input type="text" name="addr01" size="60"　value="" required>
        <div class="invalid-feedback">住所(市町村・番地)が未記入です</div>
    </div>
    <div class="mb-3">
        <label for="address">マンション等の建物名(任意)</label>
        <input type="text" name="" class="form-control" id="address" placeholder="" value="" >
    </div>
    <div class="mb-3">
        <label for="phone-number">電話番号</label>
        <input type="text" class="form-control" id="address" placeholder="ハイフンなし" value="" required>
        <div class="invalid-feedback">電話番号が未記入です</div>
    </div>
    <hr class="mb-4">

    <h4 class="mb-3">・お支払い</h4>
    <div class="d-black my-3">
    <div class="custom-control custom-radio">
        <input id="credit" name="paymentMethod" type="radio" class="custom-control-input" value="1" checked required>
        <label class="custom-control-label" for="credit">クレジットカード</label>
        </div>
    <div class="custom-control custom-radio">
        <input id="debit" name="paymentMethod" type="radio" class="custom-control-input" value="2" required>
        <label class="custom-control-label" for="debit">デビットカード</label>
        </div>
    <div class="custom-control custom-radio">
        <input id="paypal" name="paymentMethod" type="radio" class="custom-control-input" value="3" required>
        <label class="custom-control-label" for="paypal">paypal</label>
        </div>
    </div>
    <div class="row">
    <div class="col-md-6 mb-3">
        <label for="cc-name">カード名義人</label>
        <input type="text" name="" class="form-control" id="name" placeholder="例）yamada taro" value="" required>
        <small class="text-muted">カードに表示されている氏名</small>
        <div class="invalid-feedback">氏名が未記入です</div>
    </div>
    <div cvlass="col-md-6 mb-3">
        <label for="cc-number">カード番号</label>
        <input type="text" name="" class="form-control" id="number" placeholder="16桁の数字" value="" maxlength='16' pattern="^[0-9A-Za-z]+$" required>
        <div class="invalid-feedback">カード番号が未記入です</div>
    </div>
    </div>
    <div class="row">
    <div class="col-md-5 mb-3">
        <label for="cc-expiration">有効期限</label>
        <input type="text" name="" class="form-control" id="expiration" maxlength='5' placeholder="例）22/04" value="" pattern="^[0-9A-Za-z]+$" required>
        <div class="invalid-feedback">有効期限が未記入です</div>
    </div>
    <div class="col-md-5 mb-3">
        <label for="cc-cvv">CVV(セキュリティーコード)</label>
        <input type="password" name="" class="form-control" id="cvv" placeholder="3桁の数字" value="" maxlength='3' pattern="^[0-9A-Za-z]+$"　required>
        <div class="invalid-feedback">セキュリティーコードが未記入です</div>
        </div>
    </div>
    <hr class="mb-4">
        <p><button class="btn-flat-simple btn-lg bin btn-lg btn-block" type="submit">確認</button></p>
</form>
        </div>
    </div>
</div>
<!-- footer -->
<footer>
　　<ul class="footer-menu">
     <li>home ｜</li>
     <li>about ｜</li>
     <li>service ｜</li>
     <li>Contact Us</li>
    </ul>
    <p>© All rights reserved by webcampnavi.</p>
</footer>
</body>
</html>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>商品詳細</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/header.css">
<link rel="stylesheet" href="css/footer.css">
<link rel="stylesheet" href="css/input-group.css">
<link rel="stylesheet" href="css/cart.css">

</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />

	<main role="main">
	<div class="album py-5 bg-light">
		<div class="container">
			<h3 class=" col-12 light" align="center">商品詳細</h3>

			<%--ItemSearchから商品詳細に遷移した場合、”検索結果へ”のボタンを表示 --%>
			<%--Adminから商品詳細に遷移した場合、Adminへ戻るボタンを表示 --%>
			<a href=" itemsearch.html" class="btn-flat-simple btn-lg">検索結果へ</a> <br>
			<br>

			<div class="body_top">
				<div class="row">
					<div class="col-7">
						<div class="card">
							<div>
								<img class="cardImg" src="img/${item.fileName}">
							</div>
						</div>
					</div>
					<div class="col-5">
						<div class="">
						<form action="ItemAdd" method="post">
							<ul class="list-group mb-3">
								<li class="list-group-item d-flex justify-content-between ln-condensed">
									<div>
										<h6>商品名</h6>
										<p >${item.name }</p>
									</div>
								</li>
								<li class="list-group-item d-flex justify-content-between ln-condensed">
									<div>
										<h6>個数</h6>
									</div>
									<div class="cp_ipselect cp_sl04">
										<p>
											<select name="count" required>
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="4">4</option>
												<option value="5">5</option>
											</select>個
										</p>
									</div>
								</li>
								<li class="list-group-item d-flex justify-content-between ln-condensed">
									<div>
										<h6>単価</h6>
									</div>
										<span>${item.formatPrice }円</span>
								</li>
							</ul>
								<input type="hidden" name="item_id" value="${item.id}">
								<button type="submit" class="btn-flat-simple btn-lg bin btn-lg btn-block" id="cart_add">カートに追加</button>
							</form>
						</div>
					</div>
				</div>
			</div>
			<div class="body_top">
				<div class="row">
					<%--アイテムの詳細情報 --%>
					<h5>商品説明</h5>
					<p>${item.detail }</p>
				</div>
			</div>
		</div>
	</div>
	</main>
	<jsp:include page="/baselayout/footer.jsp" />
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー情報更新/完了</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/header.css">
<link rel="stylesheet" href="css/footer.css">
<link rel="stylesheet" href="css/resist.css">
<link rel="stylesheet" href="css/normal.css">
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />
	<div class="container">
		<div class="col-6 mx-auto">
			<div class="col10 mx-auto">
				<h2>更新が完了しました</h2>

				<div class="input-group mb-3">
					<div class="input-group-prepend">
						<span class="input-group-text" id="user-name">ユーザー名</span>
					</div>
					<label class="form-control">${udb.name}</label>
				</div>

				<div class="input-group mb-3">
					<div class="input-group-prepend">
						<span class="input-group-text" id="login-id">ログインID</span>
					</div>
					<label class="form-control">${udb.loginId}</label>
				</div>

				<div class="input-group mb-3">
					<div class="input-group-prepend">
						<span class="input-group-text" id="user-address">住所</span>
					</div>
					<label class="form-control">${udb.address}</label>
				</div>

				<hr class="mb-4">
				<div class="row">
					<div class="col s12">
						<h5>上記内容で更新しました。</h5>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-6 mx-auto">
						<a href="Index"
							class="btn-flat-simple btn-lg btn btn-lg btn-block">ユーザー情報へ</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="/baselayout/footer.jsp" />
</body>
</html>
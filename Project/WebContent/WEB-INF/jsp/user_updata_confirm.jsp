<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー情報更新/確認</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/header.css">
<link rel="stylesheet" href="css/footer.css">
<link rel="stylesheet" href="css/resist.css">
<link rel="stylesheet" href="css/normal.css">
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />
	<div class="container">
		<div class="col-6 mx-auto">
			<div class="col10 mx-auto">
				<div style="text-align: center">
					<h2>更新内容の確認</h2>
				</div>
				<form action="UserUpdataResult" method="post">

					<%--ユーザーをtype="hidden"で取得 --%>
					<input type="hidden" name="id" value=${udb.id }>

					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text" id="user-name">ユーザー名</span>
						</div>
						<input type="text" name="name" class="form-control" placeholder="ユーザー名" value="${ udb.name}" readonly>
					</div>

					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text" id="login-id">ログインID</span>
						</div>
						<input type="text" name="loginId" class="form-control" placeholder="ログインID" value="${udb.loginId }" readonly>
					</div>

					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text" id="user-address">住所</span>
						</div>
						<input type="text" name="address" class="form-control" placeholder="住所" value="${udb.address }" readonly>
					</div>

						<%--パスワードをtype="hidden"で取得 --%>
						<input type="hidden" name="password" class="form-control" placeholder="パスワード" value="${udb.loginIdPassword }" readonly>
					</div>

					<hr class="mb-4">
					<div class="row">
						<div class="col s12">
							<h5>上記内容で更新してよろしいでしょうか?</h5>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col6 mx-auto">
							<button type="submit" class="btn-flat-simple btn-lg " name="confirm_button" value="cancel">修正</button>
						</div>
						<div class="col6 mx-auto">
							<button type="submit" class="btn-flat-simple btn-lg " name="confirm_button" value="updata">更新</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<jsp:include page="/baselayout/footer.jsp" />
</body>
</html>
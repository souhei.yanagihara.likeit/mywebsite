<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー情報</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/header.css">
<link rel="stylesheet" href="css/footer.css">
<link rel="stylesheet" href="css/input-group.css">
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />
	<main role="main">
	<div class="album py-5 bg-light">
		<div class="container">
			<h3 class=" col-12 light" style="text-align: center">ユーザー情報</h3>
			<div class="body_top">
				<h4>・詳細</h4>
				<div class="row">
					<div class="input-field col s6">
						<div class="userdata-group">
							<span id="basic-addon">ユーザー名</span>
						</div>
						<div type="text" class="form-control" id="user-name" name="" value=""></div>
					</div>
					<div class="input-field col s6">
						<div class="userdata-group">
							<span id="basic-addon">ログインID</span>
						</div>
						<div type="text" class="form-control" id="loginID" name="" value=""></div>
					</div>
				</div>
				<div class="input-field">
					<div class="userdata-group">
						<span id="basic-addon">住所</span>
					</div>
					<div type="text" class="form-control" id="address" name="" value=""></div>
				</div>
				<br>
				<nav class="user_data">
					<ul>
						<li><a href="user_detail.html">詳細</a></li>
						<li><a href="user_updata.html">編集</a></li>
						<li><a href="user_delete.html">削除</a></li>
					</ul>
				</nav>
			</div>
		</div>

		<div class="container">
			<div class="body_top">
				<h4>・配送先</h4>
				<nav>
					<ul>
						<li><a href="shipping_address_management.html">詳細</a></li>
						<li><a href="shipping_address_resist2.html">追加</a></li>
						<li><a href="shipping_address_updata.html">編集</a></li>
						<li><a href="shipping_address_delete.html">削除</a></li>
					</ul>
				</nav>
			</div>
		</div>

		<div class="container">
			<div class="body_top">
				<h4>・お支払い</h4>
				<nav>
					<ul>
						<li><a href="payment_management.html">詳細</a></li>
						<li><a href="payment_resist.html">追加</a></li>
						<li><a href="payment_updata.html">編集</a></li>
						<li><a href="payment_delete.html">削除</a></li>
					</ul>
				</nav>
			</div>
		</div>

		<div class="container">
			<div class="body_top">
				<h4>・購入履歴</h4>
				<table class="table table-striped">
					<thead>
						<tr>
							<th></th>
							<th>購入日時</th>
							<th>支払方法</th>
							<th>配送方法</th>
							<th>購入金額</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="" items="" varStatus="status">
							<tr>
								<th><a href="">1</a></th>
								<td>2020年02月05日12時44分</td>
								<td>クレジットカード</td>
								<td>通常配送</td>
								<td>○○円</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	</main>
	<jsp:include page="/baselayout/footer.jsp" />
</body>
</html>
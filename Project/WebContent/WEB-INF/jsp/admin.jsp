<%@ page language="java" contentType="text/html; charset=UTF-8"pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>管理者｜TOPページ</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" href="css/header.css">
<link rel="stylesheet" href="css/footer.css">
<link rel="stylesheet" href="css/cart.css">
<link rel="stylesheet" href="css/checkbox.css">
<link rel="stylesheet" href="css/index.css">
<link rel="stylesheet" href="css/search.css">
</head>
<body>

<!-- header -->
<header>
  	<h1 class="headline">
      <a href="index.html" id="home-tag">ECサイト</a>
    </h1>
    <ul class="nav-list">
        <!--管理者がログインしているときに限り表示される-->
         <a href="admin.html"><li class="nav-list-item" id="home-tag">admin</li></a>
      <li class="nav-list-item">
        <a href="userdata.html" id="home-tag">user</a>
      </li>
        <a href="cart.html"><li class="nav-list-item" id="home-tag">cart</li></a>
        <a href="logout.html"><li class="nav-list-item" id="home-tag">logout</li></a>
    </ul>
</header>

<main role="main">
<div id="search-area">
    <div class="container">
        <h2 id="site-name">管理者用</h2>
        <form id="form1" action="" method="">
            <input id="sbox"  id="s" name="s" type="text" placeholder="キーワードを入力" />
            <input id="sbtn" type="submit" value="検索" />
        </form>
    </div>
</div>

<div id="side-bar">
    <div class="container">
        <nav id="side-section">
            <ul id="side-area">
            <li>
                <a href="user_list.html">ユーザーリスト</a>
            </li>
            <br>
            <li>
                <a href="item_resist.html">商品登録</a>
            </li>
            <br>
            <li>
                <a href="item_delete_list.html">削除リスト</a>
            </li>
            <br>
            <li>
                登録履歴(実装未定)
            </li>
            <br>
            <li>
                削除履歴(実装未定)
            </li>
            </ul>
        </nav>
    </div>
</div>

<div id="item-area">
<div class="section">
     <h3>登録した商品</h3>
        <br>
        <div class="row">
          <div class="col-4">
              <div class="card mb-4 shadow-sm">
                <a href="admin_item_detail.html"><svg class="bd-placeholder-img card-img-top" width="100%" height="225" xmlns="http://www.w3.org/2000/svg"           preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: Thumbnail">
                    <rect width="100%" height="100%" fill="#55595c"/>
                    <text x="50%" y="50%" fill="#eceeef" dy=".3em">商品の画像1</text>
                </svg></a>
                    <div class="item-body">
                        <p class="item-name">商品名</p>
                        <p class="item-detail">商品詳細</p>
                            <div class="d-flex justify-content-between align-items-center">
                                <p class="item-price">○○円</p>
                            </div>
                    </div>
                </div>
            </div>

         <div class="col-4">
            <div class="card mb-4 shadow-sm">
                <svg class="bd-placeholder-img card-img-top" width="100%" height="225" xmlns="http://www.w3.org/2000/svg"           preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: Thumbnail">
                    <rect width="100%" height="100%" fill="#55595c"/>
                 <text x="50%" y="50%" fill="#eceeef" dy=".3em">商品の画像2</text>
                </svg>
                <div class="item-body">
                 <p class="item-name">商品名</p>
                    <p class="item-detail">商品詳細</p>
                  <div class="d-flex justify-content-between align-items-center">
                    <p class="item-price">○○円</p>
                  </div>
                </div>
            </div>
         </div>

         <div class="col-4">
          <div class="card mb-4 shadow-sm">
            <svg class="bd-placeholder-img card-img-top" width="100%" height="225" xmlns="http://www.w3.org/2000/svg"           preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: Thumbnail">
                <rect width="100%" height="100%" fill="#55595c"/>
            　<text x="50%" y="50%" fill="#eceeef" dy=".3em">商品の画像3</text>
            </svg>
                <div class="item-body">
                 <p class="item-name">商品名</p>
                    <p class="item-detail">商品詳細</p>
                  <div class="d-flex justify-content-between align-items-center">
                    <p class="item-price">○○円</p>
                  </div>
                </div>
            </div>
        </div>
        </div>
    </div>
    </div>

<div id="item-area">
    <div class="section">
     <h3>削除した商品(実装未定)</h3>
        <br>
        <div class="row">
          <div class="col-4">
              <div class="card mb-4 shadow-sm">
                <a href="item.html"><svg class="bd-placeholder-img card-img-top" width="100%" height="225" xmlns="http://www.w3.org/2000/svg"           preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: Thumbnail">
                    <rect width="100%" height="100%" fill="#55595c"/>
                    <text x="50%" y="50%" fill="#eceeef" dy=".3em">商品の画像1</text>
                </svg></a>
                    <div class="item-body">
                        <p class="item-name">商品名</p>
                        <p class="item-detail">商品詳細</p>
                        <div class="d-flex justify-content-between align-items-center">
                                <p class="item-price">○○円</p>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</main>


<!-- footer -->
<footer>
　　<ul class="footer-menu">
     <li>home ｜</li>
     <li>about ｜</li>
     <li>service ｜</li>
     <li>Contact Us</li>
    </ul>
    <p>© All rights reserved by webcampnavi.</p>
</footer>
</body>
</html>
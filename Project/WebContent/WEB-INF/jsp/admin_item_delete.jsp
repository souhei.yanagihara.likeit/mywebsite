<%@ page language="java" contentType="text/html; charset=UTF-8"pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>管理者｜商品削除</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" href="css/header.css">
<link rel="stylesheet" href="css/footer.css">
<link rel="stylesheet" href="css/input-group.css">
<link rel="stylesheet" href="css/cart.css">

</head>
<body>
<!-- header -->
<header>
  	<h1 class="headline">
      <a href="index.html" id="home-tag">ECサイト</a>
    </h1>
    <ul class="nav-list">
      <li class="nav-list-item">
        <a href="userdata.html" id="home-tag">user</a>
      </li>
        <a href="cart.html"><li class="nav-list-item" id="home-tag">cart</li></a>
        <a href="logout.html"><li class="nav-list-item" id="home-tag">logout</li></a>
    </ul>
</header>

<main role="main">
<div class="album py-5 bg-light">
<div class="container">
    <h3 class=" col-12 light" align="center">商品情報の削除</h3>
     <a href=" itemsearch.html" class="btn-flat-simple btn-lg"></a>
    <br>
    <br>
     <div class="d-flex justify-content-between align-items-center">
    <h4>・商品名</h4>
    <form action="" method="">
    <button type="button"  class="btn-flat-simple btn-lg" >削除</button>
    </form>
    </div>
    <div class="body_top">
			<div class="row">
				<div class="col-7">
					<div class="card">
						<div class="card-image">
							 <svg class="bd-placeholder-img card-img-top" width="100%" height="300" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: Thumbnail">
                                <rect width="100%" height="100%" fill="#55595c"/>
                                <text x="50%" y="50%" fill="#eceeef" dy=".3em">商品の画像1</text>
                            　</svg>
				　　　　　</div>
                    </div>
                </div>
            <div class="col-5">
           			<div classs="col-md-4 order-md-2 mb-4">
                        <ul class="list-group mb-3">
                            <li class="list-group-item d-flex justify-content-between ln-condensed">
                                <div><h6 >商品名</h6></div>
                                    <span class="text-muted">〇〇</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between ln-condensed">
                                <div><h6>単価</h6></div>
                                    <span>〇〇円</span>
                            </li>
                        </ul>
                    </div>
            </div>
        </div>
    </div>
        <div class="body_top">
            <div class="row">
                <p>商品の説明</p>
            </div>
        </div>
    </div>
</div>
</main>

<!-- footer -->
<footer>
　　<ul class="footer-menu">
     <li>home ｜</li>
     <li>about ｜</li>
     <li>service ｜</li>
     <li>Contact Us</li>
    </ul>
    <p>© All rights reserved by webcampnavi.</p>
</footer>
</body>
</html>
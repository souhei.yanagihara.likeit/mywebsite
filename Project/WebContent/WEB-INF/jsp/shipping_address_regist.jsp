<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>配送先住所/登録</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/header.css">
<link rel="stylesheet" href="css/footer.css">
<link rel="stylesheet" href="css/normal.css">
<script src="https://ajaxzip3.github.io/ajaxzip3.js" charset="UTF-8"></script>

</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />
	<div class="container">
		<div class="col-6 mx-auto">
			<div class="col10 mx-auto">
				<h2 class="mb-3">配送先住所</h2>

				<form action="ShippingAddreassConfirm" method="post">
					<input type="hidden" name="id" value=${user.id }>
					<div class="row">
						<div class="col-md-5 mb-3">
							<label for="lastName">姓</label>
								<input type="text" name="lastName" class="form-control" id="lastName" placeholder="例）山田" value="${sadb.lastName }" required>
							<div class="invalid-feedback">姓が未記入です</div>
						</div>
						<div class="col-md-6 mb-3">
							<label for="firstName">名</label>
								<input type="text" name="firstName" class="form-control" id="firstName" value="${sadb.firstName}" placeholder="例）太郎" required>
							<div class="invalid-feedback">名が未記入です</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-5 mb-3">
							<label for="lastName">セイ</label>
								<input type="text" name="lastNameKatakana" class="form-control" id="lastName" value="${sadb.lastNameKatakana }" placeholder="例）ヤマダ" pattern="^[ァ-ン]+$" required>
							<div class="invalid-feedback">セイが未記入です</div>
						</div>
						<div class="col-md-6 mb-3">
							<label for="firstName">メイ</label>
								<input type="text" name="firstNameKatakana" class="form-control" id="firstName" value="${sadb.firstNameKatakana }" placeholder="例）タロウ" pattern="^[ァ-ン]+$" required>
							<div class="invalid-feedback">メイが未記入です</div>
						</div>
					</div>
					<div class="mb-3">
						<label for="email">メールアドレス</label>
							<input type="email" name="email" class="form-control" id="email" placeholder="info@example.com" value="${sadb.mailAddress }" required>
						<div class="invalid-feedback">メールアドレスが未記入です</div>
					</div>
					<div class="row">
						<div class="col-md-5 mb-3">
							<label>郵便番号(ハイフンもOK)</label>
								<input type="text" name="zip01" size="10" maxlength="8" onKeyUp="AjaxZip3.zip2addr(this,'','pref01','addr01');" value="${sadb.postalCode }" pattern="^[0-9A-Za-z]+$" required>
							<div class="invalid-feedback">郵便番号が未記入です</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-5 mb-3">
							<label for="address01">都道府県<span class="text-muted">(自動入力)</span></label>
								<input type="text" name="pref01" size="20"  value="${sadb.prefectures }" required>
							<div class="invalid-feedback">住所(都道府県)が未記入です</div>
						</div>
					</div>
					<div class="mb-3">
						<label for="address02">都道府県以降の住所<span class="text-muted">(自動入力)</span></label>
							<input type="text" name="addr01" size="60"  value="${sadb.cities }" required>
						<div class="invalid-feedback">住所(市町村・番地)が未記入です</div>
					</div>
					<div class="mb-3">
						<label for="address">マンション等の建物名</label>
							<input type="text" name="building" class="form-control" id="address" placeholder="" value="${sadb.buildingName }">
					</div>
					<div class="mb-3">
						<label for="phone-number">電話番号</label>
							<input type="text" name="phone_number" class="form-control" id="address" placeholder="ハイフンなし"
									pattern="\d{2,4}-?\d{2,4}-?\d{3,4}"  value="${sadb.phoneNumber }" required>
						<div class="invalid-feedback">電話番号が未記入です</div>
					</div>
					<hr class="mb-4">
					<p>
						<button class="btn-flat-simple btn-lg bin btn-lg btn-block" type="submit">確認</button>
					</p>
				</form>
			</div>
		</div>
	</div>
	<jsp:include page="/baselayout/footer.jsp" />
</body>
</html>
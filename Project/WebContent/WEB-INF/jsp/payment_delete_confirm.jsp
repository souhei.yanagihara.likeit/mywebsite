<%@ page language="java" contentType="text/html; charset=UTF-8"pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>お支払い方法/削除の確認</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" href="css/header.css">
<link rel="stylesheet" href="css/footer.css">
<link rel="stylesheet" href="css/normal.css">
    </head>
        <body>
<!-- header -->
<header>
<h1 class="headline">
      <a>ECサイト</a>
</h1>
    <ul class="nav-list">
      <li class="nav-list-item">
        <a>Home</a>
      </li>
      <li class="nav-list-item">About</li>
      <li class="nav-list-item">Topic</li>
    </ul>
</header>
            <div class="container">
                <div class="col-6 mx-auto">
                    <div class="col10 mx-auto">
                        <h2 class="mb-3">内容の確認</h2>

<form action="" method="">
<div class="d-black my-3">
<div class="row">
<div class="col-md-6 mb-3">
    <label for="cc-name">選択されたお支払方法</label>
    <input type="text" name="" class="form-control" id="payname" value="" readonly>
</div>
</div>
</div>
<div class="row">
<div class="col-md-6 mb-3">
    <label for="cc-name">カード名義人</label>
    <input type="text" name="" class="form-control" id="name" value="" readonly>
</div>
<div cvlass="col-md-6 mb-3">
    <label for="cc-number">カード番号</label>
    <input type="text" name="" class="form-control" id="number" value="" readonly>
</div>
</div>
<div class="row">
<div class="col-md-5 mb-3">
    <label for="cc-expiration">有効期限</label>
    <input type="text" name="" class="form-control" id="expiration" value="" readonly>
</div>
<div class="col-md-5 mb-3">
    <label for="cc-cvv">CVV(セキュリティーコード)</label>
    <input type="password" name="" class="form-control" id="cvv" value="" readonly>
    </div>
</div>


<hr class="mb-4">
<div class="row">
    <div class="col s12">
        <h5><div style="text-align: center"><p class="center-align">上記内容を削除してよろしいでしょうか?</p></div></h5>
    </div>
</div>
<br>
<div class="row">
    <div class="col6 mx-auto button">
<button type="submit" class="btn-flat-simple btn-lg "name=""  value="">修正</button>
    </div>
    <div class="col6 mx-auto button">
<button type="submit" class="btn-flat-simple btn-lg " name=""  value="">登録</button>
    </div>
</div>
            </form>
        </div>
    </div>
</div>
<!-- footer -->
<footer>
　　<ul class="footer-menu">
     <li>home ｜</li>
     <li>about ｜</li>
     <li>service ｜</li>
     <li>Contact Us</li>
    </ul>
    <p>© All rights reserved by webcampnavi.</p>
</footer>
    </body>
</html>

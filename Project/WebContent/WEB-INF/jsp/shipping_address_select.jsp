<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>配送先選択</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/header.css">
<link rel="stylesheet" href="css/footer.css">
<link rel="stylesheet" href="css/cart.css">
<link rel="stylesheet" href="css/radio.css">
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />
	<main role="main">
	<form action="" method="">
		<div class="album py-5 bg-light">
			<div class="container">
				<h2 class="head">配送先</h2>
				<br>
				<%-- エラー表示（配送先がありません） --%>
				<div class="d-flex justify-content-between align-items-center">
					<a href="Cart" class="btn-flat-simple btn-lg">戻る</a> <a
						href="shipping_address_resist2.html" class="btn-flat-simple btn-lg" id="create-address">新しい住所を追加する</a>
				</div>
				<br>

				<c:forEach var="" items="" varStatus="">
					<div class="row">
						<div class="col-md-4">
							<div class="card mb-4 shadow-sm">
								<div class="item-body">
									<p class="item-name" name=" " value=" ">氏名</p>
									<p class="item-detail" name="" value="">住所</p>
									<div class="d-flex justify-content-between align-items-center">
										<p>
											<input type="radio" id="radio-01" class="radio-input"
												name="radio" value="" checked /> <label for="radio-01">選択</label>
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</c:forEach>
			</div>
		</div>

		<section class="jumbotron text-center">
			<div class="container">
				<div class="row kakunin-area">
					<div class="col-md-12">
						<ul class="list-group mb-3">
							<a href="" class="btn btn-info my-2">次へ</a>
						</ul>
					</div>
				</div>
			</div>
		</section>
	</form>
	</main>
	<jsp:include page="/baselayout/footer.jsp" />
</body>
</html>
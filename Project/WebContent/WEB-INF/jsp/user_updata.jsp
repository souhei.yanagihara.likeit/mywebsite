<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー情報更新</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/header.css">
<link rel="stylesheet" href="css/footer.css">
<link rel="stylesheet" href="css/resist.css">
<link rel="stylesheet" href="css/normal.css">
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />
	<div class="container">
		<div class="col-6 mx-auto">
			<div class="col10 mx-auto">
				<div style="text-align: center">
					<h2>ユーザー情報の編集</h2>
				</div>

				<form action="UserUpdataConfirm" method="post">

					<%--ユーザーIDをtype="hidden"で取得 --%>
					<input type="hidden" name="id" value=${user.id }>

					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text" id="user-name">ユーザー名</span>
						</div>
						<input type="text" name="name" class="form-control" placeholder="ユーザー名" value=${user.name }>
					</div>

					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text" id="login-id">ログインID</span>
						</div>
						<input type="text" name="loginId" class="form-control" id="white" placeholder="ログインID" pattern="^[0-9A-Za-z]+$" value=${user.loginId } readonly>
					</div>

					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text" id="user-address">住所</span>
						</div>
						<input type="text" name="address" class="form-control" placeholder="住所" value=${user.address }>
					</div>

					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text" id="password">パスワード</span>
						</div>
						<input type="password" name="password" class="form-control" placeholder="パスワード" pattern="^[0-9A-Za-z]+$" value=${user.loginIdPassword }>
					</div>

					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text" id="password-confirm">パスワード(確認)</span>
						</div>
						<input type="password" name="passwordConfirm" class="form-control" placeholder="パスワード(確認)" pattern="^[0-9A-Za-z]+$" value=${user.loginIdPassword }>
					</div>
					<hr class="mb-4">
					<p>
						<button class="btn-flat-simple btn-lg  bin btn-lg btn-block"
							type="submit">確認</button>
					</p>
				</form>
			</div>
		</div>
	</div>
	<jsp:include page="/baselayout/footer.jsp" />
</body>
</html>
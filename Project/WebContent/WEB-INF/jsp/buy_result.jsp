<%@ page language="java" contentType="text/html; charset=UTF-8"pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>購入完了</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" href="css/header.css">
<link rel="stylesheet" href="css/footer.css">
<link rel="stylesheet" href="css/cart.css">
<link rel="stylesheet" href="css/table.css">

</head>
<body>

<!-- header -->
<header>
  	<h1 class="headline">
      <a href="index.html" id="home-tag">ECサイト</a>
    </h1>
    <ul class="nav-list">
        <!--管理者がログインしているときに限り表示される-->
         <a href="admin.html"><li class="nav-list-item" id="home-tag">admin</li></a>
      <li class="nav-list-item">
        <a href="userdata.html" id="home-tag">user</a>
      </li>
        <a href="cart.html"><li class="nav-list-item" id="home-tag">cart</li></a>
        <a href="logout.html"><li class="nav-list-item" id="home-tag">logout</li></a>
    </ul>
</header>

<main role="main">

<div class="album py-5 bg-light">
<div class="container">
    <h2 class="head">購入が完了しました</h2>
    <br>
     <div class="d-flex justify-content-between align-items-center">
    <a href="index.html" class="btn-flat-simple btn-lg" >買い物を続ける</a>


    </div>
    <br>
    <br>

<h3>購入詳細</h3>
 <table border="1" class="text-center" >
  <thead>
     <tr>
    <th >商品名</th>
      <th>単価</th>
  </tr>
     </thead>
    <tbody>
<c:forEach var="" items="" >
  <tr>
    <td>〇〇</td>
      <td>○○円</td>
  </tr>
</c:forEach>
        <tr>
            <td class="center">通常配送</td>
			<td class="center">0円</td>
        </tr>
     </tbody>
</table>
</div>
</div>

<section class="jumbotron text-center">
    <div class="container">
     <table border="1">
  <tr>
    <th >購入日</th>
      <th>支払方法</th>
      <th>配送方法</th>
      <th>合計金額</th>
  </tr>
  <tr>
    <td>2020年02月04日13時44分</td>
      <td>クレジットカード</td>
      <td>通常配送</td>
      <td>3,000円</td>
  </tr>
</table>
</div>
</section>

</main>

<!-- footer -->
<footer>
　　<ul class="footer-menu">
     <li>home ｜</li>
     <li>about ｜</li>
     <li>service ｜</li>
     <li>Contact Us</li>
    </ul>
    <p>© All rights reserved by webcampnavi.</p>
</footer>
</body>
</html>
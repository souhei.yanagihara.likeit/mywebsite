<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー登録</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/header.css">
<link rel="stylesheet" href="css/footer.css">
<link rel="stylesheet" href="css/resist.css">
<link rel="stylesheet" href="css/normal.css">
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />
	<div class="container">
		<div class="col-6 mx-auto">
			<div class="col10 mx-auto">
				<h2>新規登録</h2>

				<c:if test="${errMessage != null}">
					<div class="alert alert-danger" role="alert">${errMessage}</div>
				</c:if>

				<form action="UserResistConfirm" method="post">
					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text" id="user-name">ユーザー名</span>
						</div>
						<input type="text" class="form-control" placeholder="ユーザー名" name="user_name" value="${ udb.name}" required>
					</div>

					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text" id="st-address">住所</span>
						</div>
						<input type="text" class="form-control" name="address" placeholder="住所" value="${udb.address }" required>
					</div>

					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text" id="login-id">ログインID</span>
						</div>
						<input type="text" class="form-control" name="login_id" placeholder="ログインID" pattern="^[0-9A-Za-z]+$" value="${udb.loginId }" required>
					</div>

					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text" id="password">パスワード</span>
						</div>
						<input type="password" class="form-control" placeholder="パスワード" name="password" pattern="^[0-9A-Za-z]+$" minlength="4" maxlength="8">
					</div>

					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text" id="password-confirm">パスワード(確認)</span>
						</div>
						<input type="password" class="form-control" placeholder="パスワード(確認)" name="confirm_password" pattern="^[0-9A-Za-z]+$" minlength="4" maxlength="8">
					</div>

					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text" id="birthday">生年月日</span>
						</div>
						<input type="text" class="form-control" placeholder="例)1234/56/78" name=birth_day value="${udb.birth_day }" required>
					</div>

					<%--ユーザー登録日時 --%>
					<div class="input-group mb-3">
						<input type="hidden" class="form-control" name="create_day"
							value="">
					</div>
					<hr class="mb-4">
					<p>
						<button class="btn-flat-simple btn-lg bin btn-lg btn-block" type="submit">確認</button>
					</p>
				</form>
			</div>
		</div>
	</div>
	<jsp:include page="/baselayout/footer.jsp" />
</body>
</html>
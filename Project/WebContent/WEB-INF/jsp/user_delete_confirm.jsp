<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー情報/削除</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/header.css">
<link rel="stylesheet" href="css/footer.css">
<link rel="stylesheet" href="css/resist.css">
<link rel="stylesheet" href="css/cart.css">
<link rel="stylesheet" href="css/normal.css">
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />
	<div class="container">
		<div class="col-6 mx-auto">
			<div class="col10 mx-auto">
				<h2>削除内容の確認</h2>

				<form action="UserDeleteResult" method="post">

					<%--ユーザーをtype="hidden"で取得 --%>
					<input type="hidden" name="id" value=${udb.id }>


					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text" id="basic-addon1">ユーザー名</span>
						</div>
						<input type="text" class="form-control" name="name" value=${udb.name } readonly>
					</div>

					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text" id="basic-addon1">ログインID</span>
						</div>
						<input type="text" class="form-control" name="loginId" value=${udb.loginId } readonly>
					</div>

					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text" id="basic-addon1">住所</span>
						</div>
						<input type="text" class="form-control" name="address" value=${udb.address } readonly>
					</div>

					<hr class="mb-4">
					<div class="row">
						<div class="col s12">
							<h5>上記内容を削除してよろしいでしょうか?</h5>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col6 mx-auto button">
							<button type="submit" class="btn-flat-simple btn-lg " name="confirm_button" value="cancel">キャンセル</button>
						</div>
						<div class="col6 mx-auto button">
							<button type="submit" class="btn-flat-simple btn-lg " name="confirm_button" value="delete">削除</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<jsp:include page="/baselayout/footer.jsp" />
</body>
</html>
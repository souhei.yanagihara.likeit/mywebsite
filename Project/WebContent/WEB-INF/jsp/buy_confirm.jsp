<%@ page language="java" contentType="text/html; charset=UTF-8"pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>購入内容｜確認</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" href="css/header.css">
<link rel="stylesheet" href="css/footer.css">
<link rel="stylesheet" href="css/cart.css">
<link rel="stylesheet" href="css/table.css">

</head>
<body>

<!-- header -->
<header>
  	<h1 class="headline">
      <a href="index.html" id="home-tag">ECサイト</a>
    </h1>
    <ul class="nav-list">
        <!--管理者がログインしているときに限り表示される-->
         <a href="admin.html"><li class="nav-list-item" id="home-tag">admin</li></a>
      <li class="nav-list-item">
        <a href="userdata.html" id="home-tag">user</a>
      </li>
        <a href="cart.html"><li class="nav-list-item" id="home-tag">cart</li></a>
        <a href="logout.html"><li class="nav-list-item" id="home-tag">logout</li></a>
    </ul>
</header>


<main role="main">

<div class="album py-5 bg-light">
<div class="container">
    <h2 class="head">購入内容確認</h2>
    <br>
     <div class="d-flex justify-content-between align-items-center">
    <a href="pay_shipping_method.html" class="btn-flat-simple btn-lg" >戻る</a>
    </div>
    <br>
    <br>
<form action="" method="">
<c:forEach var="item" items="" varStatus="">
<div class="row">
        <div class="col-md-4">
          <div class="card mb-4 shadow-sm">
            <svg class="bd-placeholder-img card-img-top" width="100%" height="225" xmlns="http://www.w3.org/2000/svg"           preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: Thumbnail">
                <rect width="100%" height="100%" fill="#55595c"/>
            　<text x="50%" y="50%" fill="#eceeef" dy=".3em">商品の画像</text>
            </svg>
            <div class="item-body">
             <p class="item-name">商品名</p>
            <p class="item-detail">商品詳細</p>
              <div class="d-flex justify-content-between align-items-center">
                <p class="item-price">○○円</p>
              </div>
            </div>
            </div>
        </div>
    <div class="col-md-4">
          <div class="card mb-4 shadow-sm">
            <svg class="bd-placeholder-img card-img-top" width="100%" height="225" xmlns="http://www.w3.org/2000/svg"           preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: Thumbnail">
                <rect width="100%" height="100%" fill="#55595c"/>
            　<text x="50%" y="50%" fill="#eceeef" dy=".3em">商品の画像</text>
            </svg>
            <div class="item-body">
             <p class="item-name">商品名</p>
            <p class="item-detail">商品詳細</p>
              <div class="d-flex justify-content-between align-items-center">
                <p class="item-price">○○円</p>

              </div>
            </div>
            </div>
        </div>
    <div class="col-md-4">
          <div class="card mb-4 shadow-sm">
            <svg class="bd-placeholder-img card-img-top" width="100%" height="225" xmlns="http://www.w3.org/2000/svg"           preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: Thumbnail">
                <rect width="100%" height="100%" fill="#55595c"/>
            　<text x="50%" y="50%" fill="#eceeef" dy=".3em" name="" valu="">商品の画像</text>
            </svg>
            <div class="item-body">
             <p class="item-name" name=" " value=" ">商品名</p>
            <p class="item-detail" name="" value="">商品詳細</p>
              <div class="d-flex justify-content-between align-items-center">
                <p class="item-price" name="" value="">○○円</p>

              </div>
            </div>
            </div>
        </div>
    </div>
</c:forEach>

<table border="1">
  <tr>
    <th class="table-label"></th>
    <th >選択内容</th>
  </tr>
  <tr>
    <td>・配送先</td>
      <td>東京</td>
  </tr>
  <tr>
    <td>・配送方法</td>
      <td>通常配送</td>
  </tr>
  <tr>
    <td>・支払い方法</td>
      <td>クレジットカード</td>
  </tr>
</table>
</form>
</div>
</div>


<section class="jumbotron text-center">
<div class="container">
<div class="row kingaku-area">
    <div class="col-md-12">
      <h4 class="d-flex justify-content-between align-items-center mb-3">
        <span class="text-muted" name="" value="">金額</span>
        <span class="badge badge-secondary badge-pill" name="" value="">個数を表示</span>
      </h4>
      <ul class="list-group mb-3">
        <li class="list-group-item d-flex justify-content-between">
          <span></span>
          <strong name="" value="">○○円</strong>
        </li>
            <a href="buy_result.html" class="btn btn-info my-2">購入</a>
      </ul>
     </div>
    </div>
</div>
</section>

    </main>

<!-- footer -->
<footer>
　　<ul class="footer-menu">
     <li>home ｜</li>
     <li>about ｜</li>
     <li>service ｜</li>
     <li>Contact Us</li>
    </ul>
    <p>© All rights reserved by webcampnavi.</p>
</footer>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>お支払い方法/確認</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" href="css/header.css">
<link rel="stylesheet" href="css/footer.css">
<link rel="stylesheet" href="css/normal.css">
    </head>
        <body>
<!-- header -->
<header>
  	<h1 class="headline">
      <a href="example.html" id="home-tag">ECサイト</a>
    </h1>
    <ul class="nav-list">
      <li class="nav-list-item">
        <a href="" id="home-tag">Home</a>
      </li>
        <a href=""><li class="nav-list-item" id="home-tag">About</li></a>
        <a href=""><li class="nav-list-item" id="home-tag">Topic</li></a>
    </ul>
</header>
            <div class="container">
                <div class="col-6 mx-auto">
                    <div class="col10 mx-auto">
                        <h2 class="mb-3">入力内容の確認</h2>

<form action="" method="">
<h4 class="mb-3">・請求先住所</h4>
<div class="row">
<div class="col-md-5 mb-3">
    <label for="lastName">姓</label>
    <input type="text" name="" class="form-control" id="lastName" 　value="" readonly>
</div>
<div cvlass="col-md-6 mb-3">
    <label for="firstName">名</label>
    <input type="text" name="" class="form-control" id="firstName" value="" readonly>
</div>
</div>
<div class="row">
<div class="col-md-5 mb-3">
    <label for="lastName">セイ</label>
    <input type="text" name="" class="form-control" id="lastName" value=""  readonly>
</div>
<div cvlass="col-md-6 mb-3">
    <label for="firstName">メイ</label>
    <input type="text" name="" class="form-control" id="firstName" value=""  readonly>
</div>
</div>
<div class="mb-3">
    <label for="email">メールアドレス</label>
    <input type="email" name="" class="form-control" id="email" value="" readonly>
</div>
<div class="row">
<div class="col-md-5 mb-3">
    <label for="postal-code">郵便番号</label>
    <input type="text" name="" class="form-control" id="postalCode" value=""  readonly>
</div>
</div>
<div class="row">
<div class="col-md-5 mb-3">
    <label for="address">都道府県</label>
    <input type="text" name="" class="form-control" id="address" value=""  readonly>
</div>
</div>
<div class="mb-3">
    <label for="address">都道府県以降の住所</label>
    <input type="text" name="" class="form-control" id="address" value="" readonly>
</div>
<div class="mb-3">
     <label for="address">マンション等の建物名</label>
    <input type="text" name="" class="form-control" id="address" value="" readonly>
</div>
<div class="mb-3">
    <label for="phone-number">電話番号</label>
    <input type="text" name="" class="form-control" id="tel-address" value="" readonly>
</div>
<hr class="mb-4">

<h4 class="mb-3">・お支払い</h4>
<div class="d-black my-3">
<div class="row">
<div class="col-md-6 mb-3">
    <label for="cc-name">選択されたお支払方法</label>
    <input type="text" name="" class="form-control" id="cc-name" value="" readonly>
</div>
</div>
</div>
<div class="row">
<div class="col-md-6 mb-3">
    <label for="cc-name">カード名義人</label>
    <input type="text" name="" class="form-control" id="cc-name" value="" readonly>
    <small class="text-muted">カードに表示されている氏名</small>
</div>
<div cvlass="col-md-6 mb-3">
    <label for="cc-number">カード番号</label>
    <input type="text" name="" class="form-control" id="cc-number" value="" readonly>
</div>
</div>
<div class="row">
<div class="col-md-5 mb-3">
    <label for="cc-expiration">有効期限</label>
    <input type="text" name="" class="form-control" id="cc-expiration" value="" readonly>
</div>
<div class="col-md-5 mb-3">
    <label for="cc-cvv">CVV(セキュリティーコード)</label>
    <input type="password" name="" class="form-control" id="cc-cvv" value="" readonly>
    </div>
</div>

<hr class="mb-4">
<div class="row">
    <div class="col s12">
        <h5><div style="text-align: center"><p class="center-align">上記内容で登録してよろしいでしょうか?</p></div></h5>
    </div>
</div>
<div class="row">
    <div class="col6 mx-auto">
<button type="submit" class="btn-flat-simple btn-lg "name=""  value="">修正</button>
    </div>
    <div class="col6 mx-auto">
<button type="submit" class="btn-flat-simple btn-lg " name=""  value="">登録</button>
    </div>
</div>
            </form>
        </div>
    </div>
</div><!-- footer -->
<footer>
　　<ul class="footer-menu">
     <li>home ｜</li>
     <li>about ｜</li>
     <li>service ｜</li>
     <li>Contact Us</li>
    </ul>
    <p>© All rights reserved by webcampnavi.</p>
</footer>
    </body>
</html>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー登録/入力確認</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/header.css">
<link rel="stylesheet" href="css/footer.css">
<link rel="stylesheet" href="css/resist.css">
<link rel="stylesheet" href="css/normal.css">
</head>
<body>
	<!-- header -->
	<header>
		<h1 class="headline">
			<a>ECサイト</a>
		</h1>
		<ul class="nav-list">
			<li class="nav-list-item"><a>Home</a></li>
			<li class="nav-list-item">About</li>
			<li class="nav-list-item">Topic</li>
		</ul>
	</header>

	<div class="container">
		<div class="col-6 mx-auto">
			<div class="col10 mx-auto">
				<h2>入力内容の確認</h2>

				<form action="UserResistResult" method="post">
					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text" id="basic-addon1">ユーザー名</span>
						</div>
						<input type="text" class="form-control" name="name" placeholder="ユーザー名" value="${udb.name }" readonly>
					</div>

					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text" id="st-address">住所</span>
						</div>
						<input type="text" class="form-control" name="address" placeholder="住所" value="${udb.address }" readonly>
					</div>

					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text" id="basic-addon1">ログインID</span>
						</div>
						<input type="text" class="form-control" name="login_Id" placeholder="ログインID" value="${udb.loginId }" readonly>
					</div>

					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text" id="basic-addon1">パスワード</span>
						</div>
						<input type="password" class="form-control"  name="password" placeholder="パスワード" value="${udb.loginIdPassword }" readonly>
					</div>

					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text"  id="basic-addon1">生年月日</span>
						</div>
						<input type="text" class="form-control" name="birth_day" value="${udb.birthday }" readonly>
					</div>

					<%--ユーザー登録日時 --%>
					<div class="input-group mb-3">
						<div class="input-group-prepend"></div>
						<input type="hidden" class="form-control" name="create_day" value="登録日時">
					</div>

					<hr class="mb-4">
					<div class="row">
						<div class="col s12">
							<h5>上記内容で登録してよろしいでしょうか?</h5>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col6 mx-auto">
							<button type="submit" class="btn-flat-simple btn-lg " name="confirm_button" value="cancel">修正</button>
						</div>
						<div class="col6 mx-auto">
							<button type="submit" class="btn-flat-simple btn-lg " name="confirm_button" value="regist">登録</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- footer -->
	<footer>
		<ul class="footer-menu">
			<li>home ｜</li>
			<li>about ｜</li>
			<li>service ｜</li>
			<li>Contact Us</li>
		</ul>
		<p>© All rights reserved by webcampnavi.</p>
	</footer>
</body>
</html>
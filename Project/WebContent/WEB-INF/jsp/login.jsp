<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ログイン画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/login.css">
<link rel="stylesheet" href="css/header.css">
<link rel="stylesheet" href="css/footer.css">
<link rel="stylesheet" href="css/cart.css">

</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />
	<div class="container">
		<div class="col-6 mx-auto">
			<div class="col-8 mx-auto">
				<c:if test="${errorMessage != null}">
					<div class="alert alert-danger" role="alert">${errorMessage}</div>
				</c:if>
				<form action="LoginResult" method="post">
					<div class="body_top">
						<div class="row">
							<div class="form-group">
								<div class="col-6">
									<label for="loginId">ログインID</label>
								</div>
								<div class="col-sm-12">
									<input type="text" name="login_id" class="form-control"  placeholder="ログインID" required>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="password">パスワード</label>
							<input type="password" class="form-control" name="password" id="password" placeholder="パスワード" pattern="^[0-9A-Za-z]+$"  required>
						</div>
						<button type="submit" class="btn-flat-simple btn-lg btn btn-lg btn-block">ログイン</button>
						<h5>
							<a href="UserResist">新規登録</a>
						</h5>
					</div>
				</form>
			</div>
		</div>
	</div>
	<jsp:include page="/baselayout/footer.jsp"/>
	</body>
</html>
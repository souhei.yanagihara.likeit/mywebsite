<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>配送先|管理</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/header.css">
<link rel="stylesheet" href="css/footer.css">
<link rel="stylesheet" href="css/input-group.css">
<link rel="stylesheet" href="css/normal.css">
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />
	<div class="container">
		<div class="col-10 mx-auto">
			<div class="col10 mx-auto">
				<h2 class="mb-3">配送先の管理</h2>
				<a href="userdata.html" class="btn-flat-simple btn-lg">戻る</a>

				<table class="table table-hover">
					<thead>
						<tr>
							<th></th>
							<th>氏名</th>
							<th>住所</th>
							<th>電話番号</th>
							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="" items="" varStatus="status">
							<tr>
								<th scope="row"><a href="ShippingAddressDetail?id=${ }">1</a></th>
								<td>山田太郎</td>
								<td>東京都</td>
								<td>1234-5678-9012</td>
								<td><a href="shipping_address_updata.html">編集</a></td>
								<td><a href="shipping_address_delete.html">削除</a></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				<a href="ShippingAddressRegist" class="btn-flat-simple btn-lg btn btn-light btn-lg btn-block">配送先住所を追加する</a>
			</div>
		</div>
	</div>
	<jsp:include page="/baselayout/footer.jsp" />
</body>
</html>
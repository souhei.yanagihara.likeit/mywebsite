<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>カート</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/header.css">
<link rel="stylesheet" href="css/footer.css">
<link rel="stylesheet" href="css/cart.css">
<link rel="stylesheet" href="css/checkbox.css">
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />
	<main role="main">

	<form action="ItemDelete" method="post">
		<div class="album py-5 bg-light">

			<div class="container">
				<!-- メッセージ  -->
				<div style="text-align: center">${cartActionMessage}</div>
				<br>
				<h2 class="head">カート内容</h2>
				<div class="d-flex justify-content-between align-items-center">
					<a href="Index" class="btn-flat-simple btn-lg">戻る</a>
					<button class="btn-flat-simple btn-lg" type="submit" name="">削除</button>
				</div>
				<br> <br>
					<div class="row">
					<c:forEach var="item" items="${cart }" varStatus="status">
						<div class="col-md-4">
							<div class="card ">
								<a href="Item?item_id=${item.id}"><img class="cardImg" src="img/${item.fileName}"></a>
								<div class="item-body">
									<p class="item-name">${item.name }</p>
									<div class="d-flex justify-content-between align-items-center">
										<p class="item-price">${item.formatPrice }円</p>
										<p>
											<input type="checkbox" id="${status.index}" name="delete_item_id_list" value="${item.id }">
											<label for="${status.index}" class="checkbox01">削除</label>
										</p>
									</div>
								</div>
							</div>
						</div>
						<br>
						</c:forEach>
					</div>
				</div>
			</div>

		<section class="jumbotron text-center">
			<div class="container">
				<div class="row kingaku-area">
					<div class="col-md-12">
						<h4 class="d-flex justify-content-between align-items-center mb-3">
							<label class="text-muted" >金額(配送料を除く)</label>
							<span class="badge badge-secondary badge-pill"  >${totalCount }個</span>
						</h4>
						<ul class="list-group mb-3">
							<li class="list-group-item d-flex justify-content-between" >
								<span ></span>
								<strong>${totalprice }円</strong>
							</li>
							<a href="ShippingAddressSelect" class="btn btn-info my-2">次へ</a>
						</ul>
					</div>
				</div>
			</div>
		</section>
	</form>
	</main>
	<jsp:include page="/baselayout/footer.jsp" />
</body>
</html>